/// <reference path="./../jquery.d.ts"/>

/**
 * AjaxForm options
 * In typescript we're using interfaces to describe data structure or AJAX request/responses
 */
interface AjaxFormOptions
{
    /**
     * Width
     */
    'width'?: any;

    /**
     * Enabled/disabled default submit behaviour
     */
    'disableAjaxHandler'?: boolean;

    /**
     * Destroy form on hide event
     */
    'destroyOnHide'?: boolean;

    /**
     * Hide form by ESC
     */
    'hideOnEscape'?: boolean;

    /**
     * Do not assign default css classes to the container
     */
    'noDefaultCssClasses'?: boolean;

    /**
     * Form style (normal, string)
     */
    'style'?: string;

    /**
     * Pre-build callback
     */
    'preBuildCallback'?: (form:AjaxForm, builder:AjaxFormBuilder) => any;

    /**
     * Post-build callback
     */
    'postBuildCallback'?: (form:AjaxForm, builder:AjaxFormBuilder) => any;

    /**
     * Использовать FormData для отправки запросов
     */
    'useFormData'?: boolean;

    /**
     * Overlay bgColor
     *
     */
    'bgColor'?: string;
}

/**
 * Modal form with automatically ajax-submit behaviour
 */
class AjaxForm
{
    domObject:JQuery;
    options: AjaxFormOptions;
    submitBehaviour: (form:AjaxForm) => any;
    completeCallback: (form:AjaxForm) => any;
    doneCallback: (data:any, form:AjaxForm) => any;
    failCallback: (form:AjaxForm) => any;
    errorCallback: (data:any, form:AjaxForm) => any;
    showCallback: (form:AjaxForm) => any;
    hideCallback: (form:AjaxForm) => any;
    response: any;
    validation: AjaxFormValidation;

    /**
     * Create form via DOM-object or html template
     * @param domObject
     * @param options
     */
    constructor(domObject:any, options:AjaxFormOptions = {}) {
        // Конвертация HTML-шаблона в DOM-объект
        if(typeof domObject == "string") {
            domObject = $(domObject);
        }

        // Мердж дефолтной конфигурации с переданной
        this.domObject = domObject;
        this.validation = new AjaxFormValidation();
        this.options = $.extend({
            'width': 'auto',
            'useFormData': false,
            'disableAjaxHandler': false,
            'style': 'normal',
            'destroyOnHide': false,
            'hideOnEscape': false,
            'noDefaultCssClasses': false,
            'bgColor': 'form-light',
            'preBuildCallback': function() { },
            'postBuildCallback': function() { }
        }, options);

        // Инициализация колбэков
        this.showCallback = function() { };
        this.hideCallback = function() { };

        // Передать создание формы билдеру
        var builder = new AjaxFormBuilder(this);
        builder.build();
    }

    /**
     * Returns dom object (section)
     * @returns JQuery
     */
    getDomObject() {
        return this.domObject;
    }

    /**
     * Returns forms DOM object (section>form)
     * @returns JQuery
     */
    getFormDomObject() {
        return this.domObject.find('form[data-form-attr="1"]').first();
    }

    /**
     * Show form
     */
    show() {
        this.getDomObject().show().transition("show");
    }

    /**
     * Hide form
     */
    hide() {
        if(this.options.destroyOnHide) {
            this.destroy();
        }else{
            this.getDomObject().transition("hide");
        }
    }

    /**
     * Setup show callback
     * @param callback
     */
    setShowCallback(callback:(form:AjaxForm) => any) {
        this.showCallback = callback;
    }

    /**
     * Hide show callback
     * @param callback
     */
    setHideCallback(callback:(form:AjaxForm) => any) {
        this.hideCallback = callback;
    }

    /**
     * Destroy form
     */
    destroy() {
        this.getDomObject().transition("destroy");
        this.getDomObject().remove();
    }

    /**
     * Run AJAX-query (just one specialiazed library method)
     * @param ajaxParams
     * @param options
     */
    ajax(ajaxParams:any, options:any) {
        options.ajaxParams = ajaxParams;

        if(!options.domObject) {
            options.domObject = this.getDomObject();
        }

        $().ajaxHandler(options);
    }

    /**
     * Enable/disable ajax indication
     * @param status
     */
    ajaxStatus(status:boolean) {
        this.getFormDomObject().ajaxStatus(status);
    }

    /**
     * Submit form
     * @param callback
     */
    submit(callback?: (form:AjaxForm) => any):any {
        if(callback) {
            this.submitBehaviour = callback;
        }else{
            if(typeof this.submitBehaviour != 'function') {
                throw new Error('No submit behaviour available');
            }

            return this.submitBehaviour(this);
        }
    }

    /**
     * Complete callback
     * @param callback
     */
    complete(callback?: () => any) {
        if(callback) {
            this.completeCallback = callback;
        }else{
            if(typeof this.completeCallback == "function") {
                this.completeCallback(this);
            }
        }
    }

    /**
     * Done callback
     * @param callback
     */
    done(callback?: (data:any, form:AjaxForm) => any) {
        if(callback) {
            this.doneCallback = callback;
        }else{
            if(typeof this.doneCallback == "function") {
                this.doneCallback(this.response, this);
            }
        }
    }

    /**
     * Error callback
     * @param callback
     */
    error(callback?: (data:any, form:AjaxForm) => any) {
        if(callback) {
            this.errorCallback = callback;
        }else{
            if(typeof this.errorCallback == "function") {
                this.errorCallback(this.response, this);
            }
        }
    }

    /**
     * Fail callback
     * @param callback
     */
    fail(callback?: () => any) {
        if(callback) {
            this.failCallback = callback;
        }else{
            if(typeof this.failCallback == "function") {
                this.failCallback(this);
            }
        }
    }

    /**
     * Submit behaviour
     */
    sendAJAX() {
        if(this.options.useFormData) {
            this._sendAJAX_FormData();
        }else{
            this._sendAJAX_Simple();
        }
    }

    /**
     * Submit behaviour (x-www-form-urlencoded)
     * @private
     */
    _sendAJAX_Simple()
    {
        var self = this;
        var ajaxParams = {
            'url': this.getFormDomObject().attr('action'),
            'type': this.getFormDomObject().attr('method'),
            'dataType': 'json',
            'data': self.getFormDomObject().serialize()
        };

        this.ajax(ajaxParams, {
            'complete': function() {
                self.complete();
            },
            'done': function(data) {
                self.response = data;
                self.done();
            },
            'error': function(data) {
                self.response = data;
                self.error();
            },
            'fail': function() {
                self.fail();
            }
        });
    }

    /**
     * Submit behaviour (FormData)
     * @private
     */
    _sendAJAX_FormData()
    {
        var self = this;
        var fd = new FormData(<HTMLFormElement>this.getFormDomObject()[0]);
        var ajaxParams = {
            'url': this.getFormDomObject().attr('action'),
            'type': this.getFormDomObject().attr('method'),
            'processData': false,
            'contentType': false,
            'dataType': 'json',
            'data': fd
        };

        this.ajax(ajaxParams, {
            'complete': function() {
                self.complete();
            },
            'done': function(data) {
                self.response = data;
                self.done();
            },
            'error': function(data) {
                self.response = data;
                self.error();
            },
            'fail': function() {
                self.fail();
            }
        });
    }

    /**
     * Form validation
     */
    validate() {
        var validationResult = this.validation.validate();

        if(validationResult !== true) {
            for(var n in this.validation.validateTrace) {
                if(typeof this.validation.validateTrace[n] == 'string') {
                    alert(this.validation.validateTrace[n]);
                }
            }
        }

        return validationResult;
    }

    /**
     * Enable default submit callback
     */
    enableDefaultSubmitCallback() {
        var self = this;

        this.done(function(data:any, form:AjaxForm){
            self.hide();
        });

        this.submitBehaviour = function() {
            if(self.validate()) {
                self.sendAJAX();
            }

            return false;
        }
    }

    /**
     * Disable default submit callback
     */
    disableDefaultSubmitCallback() {
        this.submitBehaviour = function() {
            return true;
        }
    }
}

/**
 * AJAX-form builder
 */
class AjaxFormBuilder
{
    /**
     * Переданныя конструктору форма
     */
    form:AjaxForm;

    constructor(form:AjaxForm) {
        this.form = form;
    }

    /**
     * Build form
     */
    build() {
        this.form.options.preBuildCallback(this.form, this);

        AjaxFormBuilder._setupCssStyle();
        AjaxFormBuilder._setupJsFiles();

        this._setupDomObject();
        this._setupTransition();
        this._setupButtons();
        this._setupSubmitBehaviour();
        this._setupEscBehaviour();

        this.form.options.postBuildCallback(this.form, this);
    }

    /**
     * Load required css-files
     * @private
     */
    static _setupCssStyle() {
        var cssUrl = window['linkPrefix'] + '/css/sass/form/form-abstract.css';

        if($('link[href*="'+cssUrl+'"]').size() == 0) {
            $('head').append('<link type="text/css" rel="stylesheet" href="'+cssUrl+'"/>');
        }
    }

    /**
     * Load required js-files
     * @private
     */
    static _setupJsFiles() {
        var jsFiles = [
            window['linkPrefix'] + '/js/jquery/jquery.ajaxStatus.js',
            window['linkPrefix'] + '/js/jquery/jquery.ajaxHandler.js',
            window['linkPrefix'] + '/js/jquery/jquery.transition.js',
        ];

        for(var n in jsFiles) {
            var requireJs = jsFiles[n];

            if($('script[src*="'+requireJs+'"]').size() == 0) {
                $('head').append('<script src="'+requireJs+'"></script>');
            }
        }
    }

    /**
     * Setup <section> dom object
     * @private
     */
    _setupDomObject() {
        if(!(this.form.options.noDefaultCssClasses)) {
            this.form.getDomObject()
                .hide()
                .addClass('ajax-forms')
                .addClass('ajax-forms-' + this.form.options.style)
                .find('form').first()
                    .attr('data-form-attr', '1')
                    .addClass('form-abstract')
            ;
        }
    }

    /**
     * jquery.transition.js setup
     * @private
     */
    _setupTransition() {
        var form = this.form;
        var width = this.form.options.width;
        var domObject = this.form.getDomObject();

        var specialBgColors = {
            'form-light': 'white',
            'form-medium': 'gray',
            'form-dark': 'black'
        };

        if(specialBgColors[form.options.bgColor]) {
            var bgColor = specialBgColors[form.options.bgColor];

            domObject.addClass('form-transition-'+bgColor);
        }else{
            var bgColor:any = form.options.bgColor;
        }

        domObject.transition({
            "backgroundColor": bgColor,
            "width": typeof width == "number" ? width : 450,
            'showEvent': function() {
                if(typeof form.showCallback == 'function') {
                    form.showCallback(form);
                }
            },
            'hideEvent': function() {
                if(typeof form.hideCallback == 'function') {
                    form.hideCallback(form);
                }
            }
        });
    }

    /**
     * Find and enable default callbacks
     * @private
     */
    _setupButtons() {
        var form = this.form;

        form.getFormDomObject().children('footer').first().find('input[type="button"],input[type="submit"]').addClass('brownbutton');

        form.getDomObject().find('input[data-form-button="hide"]').click(function(){
            form.hide();
        });

        form.getDomObject().find('input[data-form-button="show"]').click(function(){
            form.show();
        });

        form.getDomObject().find('input[data-form-button="destroy"]').click(function(){
            form.destroy();
        });

        form.getDomObject().find('input[data-form-button="submit"]').click(function(){
            form.submit();
        });
    }

    /**
     * Submit behaviour
     * @private
     */
    _setupSubmitBehaviour() {
        var self = this;

        this.form.getFormDomObject().submit(function(){
            return self.form.submit();
        });

        if(!(this.form.options.disableAjaxHandler)) {
            this.form.enableDefaultSubmitCallback();
        }
    }

    /**
     * ESC behaviour
     * @private
     */
    _setupEscBehaviour()
    {
        var form = this.form;

        if(this.form.options.hideOnEscape) {
            $(document).keyup(function(e){
                if(e.keyCode === 27) {
                    form.hide();
                }
            });
        }
    }
}

/**
 * Form Validation
 */
class AjaxFormValidation
{
    validators = [];
    validateAll = false;
    validateTrace = [];

    /**
     * Append new validator
     * @param validator
     */
    append(validator:() => any) {
        this.validators.push(validator);
    }

    /**
     * Remove validator
     * @param validator
     */
    remove(validator:() => any) {
        for(var n in this.validators) {
            if(this.validators[n] == validator) {
                this.validators[n] = null;
            }
        }
    }

    /**
     * Validate
     * @returns {*}
     */
    validate():any {
        var validationResult = true;
        this.validateTrace = [];

        for(var n in this.validators) {
            var validator = this.validators[n];

            if(typeof validator == "function") {
                var result = validator();

                this.validateTrace.push(result);

                if(typeof result != "boolean" || result == false) {
                    if(this.validateAll) {
                        validationResult = false;
                    }else{
                        return false;
                    }
                }
            }
        }

        return validationResult;
    }
}