/// <reference path="./../jquery.d.ts"/>
/// <reference path="./photouploader.ts"/>
/// <reference path="./../filemanager/preview.ts"/>

/**
 * Link parameters
 * In typescript we're using interfaces to describe data structure or AJAX request/responses
 */
interface DetailedPhotoManager_Link
{
    claimId: number;
    itemId: number;
    fileName: string;
    publicLink: string;
    title: string;
    disableOverlay?: boolean;
}

/**
 * DetailedPhotoManager
 * Show/hide icons to preview product's photos
 */
class DetailedPhotoManager
{
    /**
     * Load icons in mass order
     * @param map
     */
    load(map: DetailedPhotoManager_Link[]) {
        for(var n in map) {
            this.enable(map[n]);
        }
    }

    /**
     * Enable product's photo icon
     * @param link
     * @param container
     */
    enable(link: DetailedPhotoManager_Link, container?:JQuery) {
        if(link.claimId > 0) {
            var linksDomObject = this._getPlaceholder(link.claimId, link.itemId, container);

            linksDomObject.each(function(key, elem){
                var linkDomObject = $(elem);

                if(linkDomObject.find('a').size() == 0) {
                    $('<a href="#" title="Щелкните для просмотра фотографии позиции"><img alt="[ фото ]" src="/img/filemanager/view.png"/></a>').appendTo(linkDomObject);
                    linkDomObject.addClass('detailed-photo-icon');
                }

                linkDomObject.show().find('a').unbind('click').click(function(e){
                    e.preventDefault();

                    Filemanager_PreviewHelper.open({
                        'type': link.disableOverlay ? 'vendor' : 'image',
                        'fileName': link.fileName,
                        'publicLink': link.publicLink,
                        'vendorLink': link.publicLink,
                        'title': link.title
                    });
                });
            });
        }
    }

    /**
     * Disable product's photo icon
     * @param claimId
     * @param itemId
     */
    disable(claimId: number, itemId:number) {
        if(claimId > 0) {
            this._getPlaceholder(claimId, itemId).hide();
        }
    }

    /**
     * Create an HTML placeholder for inserting link to product's photo
     * @param claimId
     * @param itemId
     * @returns JQuery
     */
    createPlaceholder(claimId:number, itemId:number) {
        return $('<div>')
            .attr('data-attr-claim-id', claimId.toString())
            .attr('data-attr-item-id', itemId.toString())
            .attr('style', 'display:none;')
            .attr('class', 'detailed-photo-placeholder')
    }

    /**
     * Finds and returns an HTML placehold for inserting link to product's photo
     * @param claimId
     * @param itemId
     * @param container
     * @private
     */
    _getPlaceholder(claimId: number, itemId:number, container?:JQuery) {
        if(!container) {
            container = $('body');
        }

        return container.find('div.detailed-photo-placeholder[data-attr-claim-id="'+claimId.toString()+'"][data-attr-item-id="'+itemId.toString()+'"]');
    }
}

