/// <reference path="./../jquery.d.ts"/>
/// <reference path="./../filemanager/preview.ts"/>
/// <reference path="./photomanager.ts"/>

declare var showVendornameTitle;

/**
 * DetailedPhotoUploader record
 * In typescript we're using interfaces to describe data structure or AJAX request/responses
 */
interface DetailedPhotoUploader_Record_Params
{
    /**
     * Product
     */
    product: {
        itemId: number;
        claimId: number;
    };

    /**
     * Photo information
     */
    photo: {
        file: {
            isUploaded: boolean;
        };
        link: FilePreview_Helper_Options;
    };

    /**
     * Product's detailed info from depot2
     */
    depot: string[]

    /**
     * Additional data (this params is not mine code, someone changed my code)
     */
    additionalData: any;
}

/**
 * AJAX Upload Response
 */
interface DetailedPhotoUploader_UploadResponse
{
    link: FilePreview_Helper_Options;
}

/**
 * Ajax Load Response
 */
interface DetailedPhotoUploader_LoadResponse
{
    records: DetailedPhotoUploader_Record_Params[];
}

/**
 * DetailedPhotoUploader - helper for uploading products photos to the server
 *  - DetailedPhotoUploader contains an AJAX-form for uploading photos
 *  - Show, delivering changes to the page (when we uploading a photo we should change the preview icon not just in form but, for example, in claim)
 *  - Additional method to destroy/upload photos
 */
class DetailedPhotoUploader
{
    loadUrl = '/claim/ajax/detailed-photo-list';

    form: AjaxForm;
    claimId: number;
    detailedPhotoManager: DetailedPhotoManager;

    uploadCounter = 0;

    constructor(claimId:number, detailedPhotoManager: DetailedPhotoManager) {
        this.claimId = claimId;
        this.detailedPhotoManager = detailedPhotoManager;
    }

    /**
     * Open AJAX upload photos form
     */
    open() {
        var self = this;

        this.form = new AjaxForm($('script[data-form-template="detailed-photo-uploader"]').html(), {
            'width': 1100,
            'disableAjaxHandler': true,
            'style': 'strict',
            'destroyOnHide': true
        });

        this.form.setShowCallback(function(form:AjaxForm){
            var ajaxParams = {
                'url': self.loadUrl,
                'dataType': 'json',
                'type': 'POST',
                'data': {
                    'claimId': self.claimId
                }
            };

            form.ajax(ajaxParams, {
                'done': function(data:DetailedPhotoUploader_LoadResponse) {
                    if(data.records.length == 0) {
                        alert('В заявке нет товаров, к которым можно было бы прикрепить фотографию');
                        self.close();
                    }else{
                        for(var n in data.records) {
                            var record = new DetailedPhotoUploader_Record(self, data.records[n]);
                            record.append(self);
                        }
                    }
                },
                'error': function(data:any) {
                    form.destroy();
                },
                'fail': function() {
                    form.destroy();
                }
            });
        });

        this.form.submit(function(form:AjaxForm){
            self.uploadCounter = 0;

            self.getRecordsContainer().find('form').each(function(key, subForm) {
                $(subForm).submit();
            });

            self.touchSubmitButton();
        });

        this.form.show();
    }

    /**
     * Close(hide) AJAX upload photos form
     */
    close() {
        this.form.hide();
    }

    /**
     * Returns true if form is opened
     * @returns boolean
     */
    isOpened() {
        return typeof this.form == 'object';
    }

    /**
     * Returns AjaxForm
     * @returns AjaxForm
     */
    getForm() {
        return this.form;
    }

    /**
     * Returns submit button
     * @returns JQuery
     */
    getSubmitButton() {
        return this.form.getDomObject().find('input[type="submit"],input[data-form-button="submit"]');
    }

    /**
     * Check should we block submit button or not
     * Out form can upload several photos at once and we should block submit button to not let the user submit form again
     */
    touchSubmitButton() {
        if(this.uploadCounter > 0) {
            this.getSubmitButton().attr('disabled', 'disabled');
        }else{
            this.getSubmitButton().removeAttr('disabled');
        }
    }

    /**
     * Returns records container
     * @returns JQuery
     */
    getRecordsContainer() {
        return this.form.getDomObject().find('[data-form-section="records"]');
    }
}

/**
 * DetailedPhotoUploader_Record
 * Single record from form
 */
class DetailedPhotoUploader_Record
{
    static uploadUrl = window['linkPrefix'] + '/claim/ajax/detailed-photo-upload';
    static removeUrl = window['linkPrefix'] + '/claim/ajax/detailed-photo-remove';

    form: AjaxForm;
    uploader: DetailedPhotoUploader;
    params: DetailedPhotoUploader_Record_Params;
    domObject: JQuery;
    subFormDomObject: JQuery;
    detailedPhotoManager:DetailedPhotoManager;

    constructor(uploader: DetailedPhotoUploader, params: DetailedPhotoUploader_Record_Params) {
        var self = this;

        this.uploader = uploader;
        this.form = uploader.form;
        this.detailedPhotoManager = uploader.detailedPhotoManager;
        this.params = params;
        this.domObject = $($('script[data-form-template="detailed-photo-uploader-record"]').html());
        this.subFormDomObject = $($('script[data-form-template="detailed-photo-uploader-record-form"]').html());

        this._buildRecord();
        this.update();

        this.subFormDomObject.submit(function(e){
            e.preventDefault();
            e.stopPropagation();

            if(self.subFormDomObject.find('input[type="file"]').val()) {
                self.uploader.uploadCounter++;

                if(!(self.params.photo.file.isUploaded)) {
                    var formData = new FormData($(this).get(0));
                    formData.append('itemId', self.params.product.itemId);
                    formData.append('claimId', self.params.product.claimId);

                    var ajaxParams = {
                        'url': DetailedPhotoUploader_Record.uploadUrl,
                        'type': 'POST',
                        'dataType': 'json',
                        'data': formData,
                        'processData': false,
                        'contentType': false
                    };

                    self.form.ajax(ajaxParams, {
                        'domObject': $(this).parent(),
                        'complete': function() {
                            self.uploader.uploadCounter--;
                            self.uploader.touchSubmitButton();
                        },
                        'done': function(data:DetailedPhotoUploader_UploadResponse) {
                            self.params.photo.file.isUploaded = true;
                            self.params.photo.link = data.link;
                            self.update();
                        },
                        'error': function() {
                            (<HTMLFormElement>self.subFormDomObject[0]).reset();
                        },
                        'fail': function() {}
                    });
                }
            }
        });

        var viewForm = this.getFormDomObject().find('section[data-mode="view"]');

        viewForm.find('a.view-link').click(function(e){
            e.preventDefault();

            if(self.params.photo.file.isUploaded) {
                Filemanager_PreviewHelper.open(self.params.photo.link);
            }
        });

        viewForm.find('a.delete-link').click(function(e){
            e.preventDefault();

            if(confirm('Вы действительно хотите удалить эту фотографию?')) {
                self.remove();
            }
        });
    }

    /**
     * Returns formDomObject
     * @returns JQuery
     */
    getFormDomObject() {
        return this.subFormDomObject;
    }

    /**
     * Append record to the form
     * @param form
     */
    append(form:DetailedPhotoUploader) {
        form.getRecordsContainer().append(this.domObject);
    }

    /**
     * Destroy record and destroy the photo from server
     */
    remove() {
        var self = this;
        var ajaxParams = {
            'url': DetailedPhotoUploader_Record.removeUrl,
            'type': 'POST',
            'dataType': 'json',
            'data': {
                'claimId': this.params.product.claimId,
                'itemId': this.params.product.itemId
            }
        };

        this.form.ajax(ajaxParams, {
            'domObject': this.getFormDomObject().parent(),
            'done': function(data) {
                self.getFormDomObject().find('section[data-mode="view"]').hide();
                self.getFormDomObject().find('section[data-mode="upload"]').show();

                self.detailedPhotoManager.disable(self.params.product.claimId, self.params.product.itemId);
                self.params.photo.file.isUploaded = false;
            }
        });
    }

    /**
     * Upload photo to the server
     */
    upload() {
        this.getFormDomObject().submit();
    }

    /**
     * Form record builder
     * @private
     */
    _buildRecord() {
        this.domObject.find('td[data-table-attr="form-photo"]').append(this.subFormDomObject);
        this.domObject.find('td[data-table-attr="item-id"]').text(this.params.product.itemId.toString());
        this.domObject.find('td[data-table-attr="claim-id"]').text(this.params.product.claimId.toString());
    }

    /**
     * Update form view
     */
    update() {
        this.getFormDomObject().find('section[data-mode="upload"],section[data-mode="view"],section[data-mode="error"]').hide();

        if(this.params.photo.file.isUploaded) {
            this._showViewForm();
            this.detailedPhotoManager.enable({
                claimId: this.params.product.claimId,
                itemId: this.params.product.itemId,
                fileName: this.params.photo.link.fileName,
                publicLink: this.params.photo.link.publicLink,
                title: this.params.photo.link.title
            });
        }else{
            this._showUploadForm();
            this.detailedPhotoManager.disable(this.params.product.claimId, this.params.product.itemId);
        }

        for(var fieldName in this.params.depot) {
            var fieldValue = this.params.depot[fieldName];

            if(typeof fieldValue != 'string') {
                fieldValue = '—';
            }

            this.domObject.find('td[data-table-attr="'+fieldName+'"]').text(fieldValue);

            // Отображение реального названия поставщика
            if (this.params.additionalData != undefined) {
                showVendornameTitle(this.params.additionalData, this.domObject.find('td[data-table-attr="'+fieldName+'"]'), ('field' + fieldName));
            }
        }

        (<HTMLFormElement>this.subFormDomObject[0]).reset();
    }

    /**
     * Show upload sub-form
     * @private
     */
    _showUploadForm() {
        this.getFormDomObject().find('section[data-mode="upload"]').show();
    }

    /**
     * Hide view sub-form
     * @private
     */
    _showViewForm() {
        var viewForm = this.getFormDomObject().find('section[data-mode="view"]');

        viewForm.show();
        viewForm.find('a.view-link').text(this.params.photo.link.fileName);
    }
}

