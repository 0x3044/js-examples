/// <reference path="./../../jquery.d.ts"/>

/**
 * Ajax Response for /get-form query
 * In typescript we're using interfaces to describe data structure or AJAX request/responses
 */
interface Default_Penalty_Form_AjaxResponse_GetForm
{
    repository: {
        schemes: {
            name: string;
            className: string;
            title: string;
            description: string;
            subForm: boolean;
            visible: boolean;
        }[];
        watchers: {
            name: string;
            className: string;
            title: string;
            description: string;
            subForm: boolean;
            visible: boolean;
        }[];
    };
}