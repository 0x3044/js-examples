/// <reference path="./../../jquery.d.ts"/>
/// <reference path="./ajax.d.ts"/>

/**
 * New Penalty form
 */
class Default_Penalty_Form
{
    form: AjaxForm;
    elements: Default_Penalty_Form_Elements;

    constructor() {
        this._setUpForm();
        this._setUpElements();
        this._setUpValidation();
    }

    _setUpForm() {
        var that = this;
        var formParams:AjaxFormOptions = {
            width: 500,
            style: 'normal',
            destroyOnHide: true
        };

        var form = new AjaxForm($('script[data-script-type="create-penalty-form"]').html(), formParams);

        form.setShowCallback(function(){
            form.ajax(
                {
                    'url': '/default/penalty/get-form',
                    'method': 'GET',
                    'type': 'json'
                },
                {
                    'done': function(data:Default_Penalty_Form_AjaxResponse_GetForm, form:AjaxForm) {
                        that.buildForm(data);
                    }
                }
            );
        });

        form.done(function (data, form:AjaxForm) {
            form.hide();

            if(window['filterController'] instanceof Default_Penalty_FilterController) {
                window['filterController'].dataTable.reset();
            }
        });

        this.form = form;
    }

    _setUpElements() {
        var form = this.form;

        this.elements = new Default_Penalty_Form_Elements(this);
        this.elements.createButton.click(function(){
            form.show();
        });
    }

    _setUpValidation() {
        var elements = this.elements;

        this.form.validation.validateAll = true;
        this.form.validation.append(function(){
            var title = elements.titleInput.val();

            if(title.length == 0) {
                return "Отсутствует название";
            }

            return true;
        });

        this.form.validation.append(function(){
            var sumInput = $('#createFormSum');

            if(sumInput.size()) {
                var sum = parseFloat(sumInput.val());

                if(isNaN(sum) || sum < 0.01) {
                    return "Не указана сумма штрафа";
                }
            }

            return true;
        });
    }

    /**
     * Form builder
     * @param data
     */
    buildForm(data:Default_Penalty_Form_AjaxResponse_GetForm) {
        var that = this;

        this.elements.watcherSelect.find('option').remove();
        this.elements.schemeSelect.find('option').remove();

        for(var n in data.repository.watchers) {
            var record = data.repository.watchers[n];

            if(record.visible) {
                this.elements.watcherSelect.append(
                    $('<option>').val(record.name)
                        .attr('title', record.description)
                        .attr('data-has-sub-form', record.subForm ? '1' : '0')
                        .text(record.title)
                )
            }
        }

        for(var n in data.repository.schemes) {
            var record = data.repository.schemes[n];

            if(record.visible) {
                this.elements.schemeSelect.append(
                    $('<option>').val(record.name)
                        .attr('title', record.description)
                        .attr('data-has-sub-form', record.subForm ? '1' : '0')
                        .text(record.title)
                )
            }
        }

        $([this.elements.watcherSelect, this.elements.schemeSelect]).each(function(index, elem) {
            var handler = function(){
                if(parseInt($(this).val()) == 0) {
                    $(this).siblings('div.description').html('');
                }else{
                    $(this).siblings('div.description').text($(this).find('option:selected').attr('title'));
                }

                if(parseInt($(this).find('option:selected').attr('data-has-sub-form')) == 1) {
                    that.buildSubForm($(this).attr('data-sub-form-trigger'), $(this).val());
                }else{
                    that.resetSubForm($(this).attr('data-sub-form-trigger'));
                }
            };


            $(elem).keyup(handler).change(handler).change();
        });

        var enabledInput = this.elements.enabledHidden;

        this.elements.enabledCheckbox.change(function(){
            enabledInput.val($(this).is(':checked') ? '1' : '0');
        }).change();
    }

    /**
     * Sub-Form builder
     * @param type
     * @param template
     * @param noError
     */
    buildSubForm(type:string, template:string, noError = false) {
        var container = this.getSubForm(type).container;

        switch(type) {
            default:
                throw new Error('Unknown type');

            case 'watcher':
                var script = $('script[data-script-type="create-penalty-watcher-subform-'+template+'"]');
                break;

            case 'scheme':
                var script = $('script[data-script-type="create-penalty-scheme-subform-'+template+'"]');
                break;
        }

        if(!noError && script.size() == 0) {
            throw new Error('No template found for: ' + template);
        }

        this.resetSubForm(type);

        if(script.size() > 0) {
            container.append(script.html()).show();
        }
    }

    /**
     * Clear sub-form containers
     * @param type
     */
    resetSubForm(type:string) {
        this.getSubForm(type).container.html('').hide();
    }

    /**
     * Returns sub-form container
     * @param type
     * @returns {{container: JQuery, type: string}}
     */
    getSubForm(type:string):{container:JQuery; type:string} {
        switch(type) {
            default:
                throw new Error('Unknown type');

            case 'watcher':
                var container = $('#createFormWatcherSubForm');
                break;

            case 'scheme':
                var container = $('#createFormSchemeSubForm');
                break;
        }

        return {container: container, type:type};
    }
}

/**
 * Element repo
 */
class Default_Penalty_Form_Elements
{
    form: Default_Penalty_Form;
    createButton: JQuery;
    watcherSelect: JQuery;
    schemeSelect: JQuery;
    titleInput: JQuery;
    reasonTextarea: JQuery;
    enabledCheckbox: JQuery;
    enabledHidden: JQuery;

    constructor(form: Default_Penalty_Form) {
        this.form = form;
        this.createButton = $('input[data-filter-action="create"]');
        this.watcherSelect = $('#createFormWatcherSelect');
        this.schemeSelect = $('#createFormSchemeSelect');
        this.titleInput = $('#createFormTitle');
        this.reasonTextarea = $('#createFormReason');
        this.enabledCheckbox = $('#createFormEnabledCheckbox');
        this.enabledHidden = $('#createFormEnabledHidden');
    }
}

